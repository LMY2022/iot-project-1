#!/bin/bash

OUTPUT=/opt/iotproject/index.html
DIRLS=/tmp
INTERVALL=5
ZEIT=$(date)

: '
if [ $# -eq 1 ]
then
    if [ -d "$1" ]
    then
        echo "$1 exists"
        DIRLS=$1
        ls $DIRLS
    else
        echo "$1 is not a directory"
    fi
else
    echo "please specify a directory"
    exit
fi
'

while true
do
    echo "<html><body>" > $OUTPUT
    echo "<h1>Mein Webserver</h1>" >> $OUTPUT
    echo $ZEIT >> $OUTPUT
    #ls $DIRLS >> $OUTPUT
    #For Loop der die Existenz des Files zuerst prüft und dann anhängt oder Fehlermeldung ausgiebt.
    for htmlsnippet in $*
    do
        if [ -r "$htmlsnippet" ] #prüfen ob aktuelles Element in $htmlsnippet ein file ist.
        then
            echo "$htmlsnippet is a file. Outputting to $OUTPUT"
            cat $htmlsnippet >> $OUTPUT #wenn true, aktuelles Element in $OUTPUT schreiben.
        else
            echo "File $htmlsnippet is not a file." >&2 #wenn false, aktuelles Element in Standard Error (?)
        fi
    done

    echo "</body></html>" >> $OUTPUT
    sleep $INTERVALL
done